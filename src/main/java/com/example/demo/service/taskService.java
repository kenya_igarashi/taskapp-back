package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.task;
import com.example.demo.repository.ProgressRepository;
import com.example.demo.repository.TaskRepository;

@Service
public class taskService {
	@Autowired
	TaskRepository taskRepository;

	// 新規作成・編集
	public void saveTask(task task) {
		taskRepository.save(task);
	}

	// タスク全件取得
	public List<task> findAllTask() {
		return taskRepository.findByDelFlgOrderByDueDateAsc(0);

	}

	// タスク削除
	public void deleteTask(Integer id) {
		taskRepository.deleteById(id);
	}

}