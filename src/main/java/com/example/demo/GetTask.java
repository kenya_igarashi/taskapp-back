package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.task;
import com.example.demo.service.taskService;
import com.fasterxml.jackson.core.JsonProcessingException;

@RestController
@RequestMapping("task")
public class GetTask {
	@Autowired
	taskService taskService;

	@CrossOrigin
	@GetMapping(value = "/getTask")
	public ResponseEntity<List<task>> sendTask() throws JsonProcessingException {

		List<task> allTask = taskService.findAllTask();

		return ResponseEntity.ok().header("ContentType", MediaType.APPLICATION_JSON_VALUE.toString()).body(allTask);
	}
}
