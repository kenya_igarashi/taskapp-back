package com.example.demo;

import java.util.Date;

import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Configuration
@Data
public class PostJson {
	private String task;
	private Date dueDate;
	private Date createdDate;
}
