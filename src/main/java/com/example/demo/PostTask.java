package com.example.demo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.task;
import com.example.demo.service.taskService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("task")
public class PostTask {
	@Autowired
	taskService taskService;

	@CrossOrigin
	@PostMapping(value = "/postTask")
	public void postTask(@RequestBody String json) throws JsonMappingException, JsonProcessingException {

		// ObjectMapperのインスタンス生成
		ObjectMapper mapper = new ObjectMapper();
		// JsonNodeに変換。（これによりkeyを指定することでvalueを取得することができる。）
		JsonNode node = mapper.readTree(json);

		// get()で指定した文字列をkeyにvalueを取得する。
		String taskText = node.get("task").textValue();
		Date dueDate = null;
		// Date createdDate = null;
		try {
			SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd");
			dueDate = sdFormat.parse(node.get("dueDate").textValue());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		task task = new task();
		task.setTask(taskText);
		task.setDueDate(dueDate);
		task.setProgress("未着手");
		task.setDelFlg(0);
		// タスクをテーブルに格納
		taskService.saveTask(task);

	}
}
