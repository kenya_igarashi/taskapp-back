package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.task;

@Repository
public interface TaskRepository extends JpaRepository<task, Integer> {
	List<task> findByOrderByDueDateAsc();

	List<task> findByDelFlg(Integer delFlg);

	List<task> findByDelFlgOrderByDueDateAsc(Integer delFlg);
}