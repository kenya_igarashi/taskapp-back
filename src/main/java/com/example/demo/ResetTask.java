package com.example.demo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.task;
import com.example.demo.service.taskService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

@RestController
@RequestMapping("task")
public class ResetTask {
	@Autowired
	taskService taskService;

	@CrossOrigin
	@PostMapping(value = "/resetTask/{id}")
	public void deleteTask(@PathVariable("id") String id) throws JsonMappingException, JsonProcessingException {

		task task = new task();
		Date dueDate = null;
		try {
			SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd");
			dueDate = sdFormat.parse("2038-01-01");
			// createdDate = sdFormat.parse(node.get("createdDate").textValue());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		task.setId(Integer.parseInt(id));
		task.setTask("タスク");
		task.setDueDate(dueDate);
		taskService.saveTask(task);

	}
}